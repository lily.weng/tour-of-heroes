import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'
import {LayoutComponent} from './layout/layout.component'

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'index'},
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'index',
        loadChildren: () => import('./pages/home/home.module').then((m) => m.HomeModule),
      },
      {
        path: 'heros',
        loadChildren: () => import('./pages/heros/heros.module').then((m) => m.HerosModule),
      },
      {
        path: 'detail',
        loadChildren: () => import('./pages/detail/detail.module').then((m) => m.DetailModule),
      },
      {
        path: 'detail/:id',
        loadChildren: () => import('./pages/detail/detail.module').then((m) => m.DetailModule),
      },
    ],
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
