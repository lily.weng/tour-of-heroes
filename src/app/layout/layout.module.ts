import {RouterModule} from '@angular/router'
import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {LayoutComponent} from './layout.component'
import {HeaderModule} from '@app/shared/component/header/header.module'
import {FooterModule} from '@app/shared/component/footer/footer.module'

@NgModule({
  declarations: [LayoutComponent],
  imports: [CommonModule, HeaderModule, FooterModule, RouterModule],
  exports: [LayoutComponent],
})
export class LayoutModule {}
