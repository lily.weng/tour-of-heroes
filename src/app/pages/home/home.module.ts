import {MatButtonModule} from '@angular/material/button'
import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {MatInputModule} from '@angular/material/input'
import {HomeRoutingModule} from './home-routing.module'
import {HomeComponent} from './home.component'
import {MatIconModule} from '@angular/material/icon'
import {HistroyModule} from '@app/shared/component/histroy/histroy.module'

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, HomeRoutingModule, MatButtonModule, MatInputModule, MatIconModule, HistroyModule],
  exports: [HomeComponent],
})
export class HomeModule {}
