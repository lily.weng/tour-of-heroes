import {Component, OnInit} from '@angular/core'
import {Router} from '@angular/router'

@Component({
  selector: 'app-heros',
  templateUrl: './heros.component.html',
  styleUrls: ['./heros.component.scss'],
})
export class HerosComponent {
  constructor(private router: Router) {}
  goDetail() {
    this.router.navigate(['/detail'])
  }
}
