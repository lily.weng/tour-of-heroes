import {RouterModule} from '@angular/router'
import {MatButtonModule} from '@angular/material/button'
import {MatInputModule} from '@angular/material/input'
import {MatIconModule} from '@angular/material/icon'
import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'

import {HerosRoutingModule} from './heros-routing.module'
import {HerosComponent} from './heros.component'
import {HistroyModule} from '@app/shared/component/histroy/histroy.module'

@NgModule({
  declarations: [HerosComponent],
  imports: [
    CommonModule,
    HerosRoutingModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    RouterModule,
    HistroyModule,
  ],
  exports: [HerosComponent],
})
export class HerosModule {}
