import {MatButtonModule} from '@angular/material/button'
import {MatInputModule} from '@angular/material/input'
import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'

import {DetailRoutingModule} from './detail-routing.module'
import {DetailComponent} from './detail.component'
import {HistroyModule} from '@app/shared/component/histroy/histroy.module'

@NgModule({
  declarations: [DetailComponent],
  imports: [CommonModule, DetailRoutingModule, MatInputModule, MatButtonModule, HistroyModule],
  exports: [DetailComponent],
})
export class DetailModule {}
