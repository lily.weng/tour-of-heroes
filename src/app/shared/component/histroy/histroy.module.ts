import {MatIconModule} from '@angular/material/icon'
import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {HistoryComponent} from './history.component'

@NgModule({
  declarations: [HistoryComponent],
  imports: [CommonModule, MatIconModule],
  exports: [HistoryComponent],
})
export class HistroyModule {}
